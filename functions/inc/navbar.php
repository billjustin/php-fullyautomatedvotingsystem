<nav class="white">
    <div class="nav-wrapper container">
        <a href="#" class="brand-logo grey-text text-darken-3">Voting System</a>
        <ul class="right">
            
            <li><a class="grey-text text-darken-3" href="../functions/logout.php">Logout</a></li>
        </ul>
    </div>
</nav>