<?php

if (isset($_POST['position'])) {
    echo '<div class="center" style="margin-bottom: 30px;">
            <button class="uk-button uk-button-primary" type="button" uk-toggle="target: #modal-add">ADD Position</button>
            <button class="uk-button uk-button-danger" type="button" uk-toggle="target: #modal-reset">RESET TABLE</button>
            </div>';
    echo ' <table class="uk-table uk-table-small uk-table-divider uk-table-hover" style="background: white; max-width: 400px; margin: 0 auto;">';
    echo '
        <thead>
            <tr>
                <th>Name</th>
                <th>Available</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody style="text-transform: capitalize;">
        ';
    $query = mysqli_query($con, 'SELECT * FROM positions');
    while ($row = mysqli_fetch_array($query)) {
        extract($row);
        echo '
            <tr>
                <td>'. $position .'</td>
                <td>'. $available .'</td>
                <td><a href="">Edit</a> | <a>Delete</a></td> 
            </tr>
            
            ';
    }
    echo '</tbody>';
    echo '</table>';


    echo '
    <div id="modal-add" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title">Add Account</h2>
        <form class="uk-grid-small" action=""  method="post" uk-grid>
                <input class="uk-input" placeholder="Position" type="text" name="position" required><br /><br />
                <input class="uk-input" placeholder="Available" type="number" name="available" required><br /><br />
            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                <button class="uk-button uk-button-primary" type="submit" name="addPosition">Register</button>
            </p>
        </form>
        </div>
    </div>
        ';

    echo '
    <div id="modal-reset" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
        <p>Are you sure you want to erase all of the accounts data?
        <p class="uk-text-right">
        <form action="" method="post">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
            <button class="uk-button uk-button-danger" type="submit" name="resetPosition">RESET</button>
        </form>
        </p>
        </div>
    </div>
        ';

    
}
