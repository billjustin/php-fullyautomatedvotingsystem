<?php

if (isset($_POST['candidates'])) {
    echo '<div class="center" style="margin-bottom: 30px;">
            <button class="uk-button uk-button-primary" type="button" uk-toggle="target: #modal-add">ADD CANDIDATE</button>
            <button class="uk-button uk-button-danger" type="button" uk-toggle="target: #modal-reset">RESET TABLE</button>
            </div>';
    echo ' <table class="uk-table uk-table-small uk-table-divider uk-table-hover" style="background: white; max-width: 800px; margin: 0 auto;">';
    echo '
        <thead>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Position</th>
                <th>Partylist</th>
                <th>Year Level</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody style="text-transform: capitalize;">
        ';

    $sql = "SELECT 
        candidates.*, 
        pos.position, 
        pos.available, 
        plist.name
            FROM candidates 
            INNER JOIN positions as pos ON candidates.positionID = pos.id
            INNER JOIN partylist as plist ON candidates.partylist = plist.id
           ";

    $query = mysqli_query($con, $sql);
    while ($row = mysqli_fetch_array($query)) {
        extract($row);
        echo '
            <tr>
                <td>'. $firstname .'</td>
                <td>'. $lastname .'</td>
                <td>'. $position .'</td>
                <td>'. $name .'</td>
                <td>'. $yearLevel .'</td>
                <td><a href="">Edit</a> | <a>Delete</a></td> 
            </tr>
            
            ';
    }
    echo '</tbody>';
    echo '</table>';


    echo '
    <div id="modal-add" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title">Add Candidates</h2>
        <form class="uk-grid-small" action=""  method="post" uk-grid>
                <input type="hidden" name="userType" value="1">
                <input class="uk-input" placeholder="Firstname" type="text" name="firstname" required><br /><br />
                <input class="uk-input" placeholder="Lastname" type="password" name="lastname" required><br /><br />
        ';
    

    echo '<select name="position" class="uk-select">';
    echo '<option value="">Select Position</option>';
            $query = mysqli_query($con, "SELECT * FROM positions");
            while ($row = mysqli_fetch_array($query)) { 
                echo ' <option value="'. $row['id'] .'">'. $row['position'] .'</option>';
            }
    echo '</select>';    

    echo '<select name="partylist" class="uk-select">';
    echo '<option value="">Select Partylist</option>';
            $query = mysqli_query($con, "SELECT * FROM partylist");
            while ($row = mysqli_fetch_array($query)) {
                echo '<option value="'. $row['id'] .'">'. $row['name'] .'</option>';
            }
    echo '</select>';    

    echo '
                <input class="uk-input" placeholder="Year Level" type="text" name="yearLevel" required><br /><br />
                <input class="uk-input" placeholder="Photo" type="text" name="photo" required><br /><br />
            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                <button class="uk-button uk-button-primary" type="submit" name="addCandidate">Register</button>
            </p>
        </form>
        </div>
    </div>
        ';

    echo '
    <div id="modal-reset" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
        <p>Are you sure you want to erase all of the accounts data?
        <p class="uk-text-right">
        <form action="" method="post">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
            <button class="uk-button uk-button-danger" type="submit" name="resetCandidate">RESET</button>
        </form>
        </p>
        </div>
    </div>
        ';

    
}