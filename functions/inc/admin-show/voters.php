<?php

if (isset($_POST['voters'])) {
    echo '<div class="center" style="margin-bottom: 30px;">
            <button class="uk-button uk-button-primary" type="button" uk-toggle="target: #modal-add">ADD VOTER</button>
            <button class="uk-button uk-button-danger" type="button" uk-toggle="target: #modal-reset">RESET TABLE</button>
            </div>';
    echo ' <table class="uk-table uk-table-small uk-table-divider uk-table-hover" style="background: white; max-width: 800px; margin: 0 auto;">';
    echo '
        <thead>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Student ID</th>
                <th>Password</th>
                <th class="uk-table-shrink">VC</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody style="text-transform: capitalize;">
        ';
    $query = mysqli_query($con, 'SELECT * FROM accounts WHERE userType = 1');
    while ($row = mysqli_fetch_array($query)) {
        extract($row);
        echo '
            <tr>
                <td>'. $firstname .'</td>
                <td>'. $lastname .'</td>
                <td>'. $studentID .'</td>
                <td>'. $password .'</td>
                <td>'. $voteCount .'</td>
                <td><a href="">Edit</a> | <a>Delete</a></td> 
            </tr>
            
            ';
    }
    echo '</tbody>';
    echo '</table>';


    echo '
    <div id="modal-add" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
        <h2 class="uk-modal-title">Add Account</h2>
        <form class="uk-grid-small" action=""  method="post" uk-grid>
                <input type="hidden" name="userType" value="1">
                <input class="uk-input" placeholder="Student ID" type="text" name="studentID" required><br /><br />
                <input class="uk-input" placeholder="Password" type="password" name="password" required><br /><br /> 
                <input class="uk-input" placeholder="Firstname" type="text" name="firstname" required><br /><br />
                <input class="uk-input" placeholder="Lastname" type="text" name="lastname" required><br /><br />
            <p class="uk-text-right">
                <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
                <button class="uk-button uk-button-primary" type="submit" name="addAccount">Register</button>
            </p>
        </form>
        </div>
    </div>
        ';

    echo '
    <div id="modal-reset" uk-modal>
        <div class="uk-modal-dialog uk-modal-body">
        <p>Are you sure you want to erase all of the accounts data?
        <p class="uk-text-right">
        <form action="" method="post">
            <button class="uk-button uk-button-default uk-modal-close" type="button">Cancel</button>
            <button class="uk-button uk-button-danger" type="submit" name="resetAccount">RESET</button>
        </form>
        </p>
        </div>
    </div>
        ';

    
}


// candidates 


