<?php
    require ('../functions/admin/admin.php');
    require ('../functions/admin/crud.php');

    include('../functions/inc/admin-session.php');
    include('../functions/inc/admin-header.php');
    include('../functions/inc/navbar.php');
?>

    <section id="show" style="height: auto;">
        <div class="ui very padded container">
            <div class="row center" style="padding-top: 50px; margin-bottom: 50px;">
            <?php 
                include('../functions/inc/messages.php');
            ?>
            <form action="" method="post">
                <button type="submit" name="voters" class="uk-button uk-button-primary">Voters</button>
                <button type="submit" name="candidates" class="uk-button uk-button-primary">Candidates</button>
                <button type="submit" name="partylist" class="uk-button uk-button-primary">Partylists</button>
                <button type="submit" name="position" class="uk-button uk-button-primary">Position</button>
            </form>
            </div>

                <?php
                    include('../functions/inc/admin-show/voters.php');
                    include('../functions/inc/admin-show/candidates.php');
                    include('../functions/inc/admin-show/partylist.php');
                    include('../functions/inc/admin-show/position.php');
                ?>         
        </div>
    </section>
    
    <script src="../js/jquery-3.1.1.min.js"></script>
    <script src="../js/uikit.min.js"></script>
</body>
</html>