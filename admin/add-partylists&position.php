<?php
    require ('../functions/admin/admin.php');

    

    include('../functions/inc/admin-session.php');
    include('../functions/inc/admin-header.php');
    include('../functions/inc/navbar.php');
?>


    <section id="admin">
        <div class="ui very padded container">
            <div class="container">
            
                <form action="" class="ui very padded form segment raised" method="post"> 
                    <!-- partylist  -->
                    <h3 class="ui dividing header">Add PartyLists</h3>
                    <div class="field">
                        <label>PartyList Name</label>
                        <input type="text" name="partylist" required>
                    </div>
                    <?php 
                        include('../functions/inc/messages.php');
                    ?>
                    <button type="submit" name="partylistbtn" class="uk-button uk-button-primary">Add Partylist</button>
                </form>
                <form action="" method="post" class="center">
                    <button type="submit" name="resetbtn" class="uk-button uk-button-danger" value="partylist">RESET TABLE</button>
                </form>
                    <!-- positions -->
                <form action="" class="ui very padded form segment raised" method="post">
                    <h3 class="ui dividing header">Create Positions</h3>
                    <div class="two fields">
                        <div class="field twelve wide">
                            <label>Position</label>
                            <input type="text" name="position" required>
                        </div>
                        <div class="field four wide">
                            <label>Available</label>
                            <input type="number" name="available" value="1" required>
                        </div>
                    </div>
                    <?php 
                        include('../functions/inc/messages.php');
                    ?>
                    <button type="submit" name="positionbtn" class="uk-button uk-button-primary">Add Position</button>
                    
                </form>
                <form action="" method="post" class="center">
                    <button type="submit" name="resetbtn" class="uk-button uk-button-danger" value="positions">RESET TABLE</button>
                </form>
            </div>
        </div>
    </section>
    
        
</body>
</html>